## App Development Team Project Management

General tasks that don't fit into other projects

## Purpose

The App Development team at UBports drives app development by maintaining the Ubuntu Touch SDK, the QQC2 Suru Style, the development documentation, and the core apps.

- [More about the core apps](http://docs.ubports.com/en/latest/contribute/preinstalled-apps.html)
- [SDK Docs](https://api-docs.ubports.com/)
- [Suru Style](https://github.com/ubports/qqc2-suru-style/)
